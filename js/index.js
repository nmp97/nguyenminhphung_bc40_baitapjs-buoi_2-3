// EXERCISE 1
function tinhLuong() {
  let luongMotngay = document.getElementById('luong-ngay').value * 1;
  let ngayCong = document.getElementById('tong-ngay-lam').value * 1;

  document.getElementById('tong-luong').innerHTML = `${luongMotngay * ngayCong}$`;
}

// EXERCISE 2
function tinhTrungBinh() {
  let soThuNhat = document.getElementById('so-thu-nhat').value * 1;
  let soThuHai = document.getElementById('so-thu-hai').value * 1;
  let soThuBa = document.getElementById('so-thu-ba').value * 1;
  let soThuTu = document.getElementById('so-thu-tu').value * 1;
  let soThuNam = document.getElementById('so-thu-nam').value * 1;

  document.getElementById('gia-tri-trung-binh').innerHTML = `${
    (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam) / 5
  }`;
}

// EXERCISE 3
function quyDoi() {
  let usd = document.getElementById('usd').value * 1;

  document.getElementById('quy-doi').innerHTML = `${usd * 23500}$`;
}

// EXERCISE 4
function hcn() {
  let chieuDai = document.getElementById('chieu-dai').value * 1;
  let chieuRong = document.getElementById('chieu-rong').value * 1;

  document.getElementById('chu-vi').innerHTML = `${(chieuDai + chieuRong) * 2} cm`;
  document.getElementById('dien-tich').innerHTML = `${chieuDai * chieuRong} cm2`;
}

// EXERCISE 5
function total() {
  let number = document.getElementById('number').value * 1;
  let total = Math.floor(number / 10) + (number % 10);

  document.getElementById('total').innerHTML = `${total}`;
}
